export class Message {
    constructor(message){
        this.message = message;
        this.date = new Date();
    }
    getInfo(){
        return JSON.stringify({
            message:this.message,
            date:this.date
        })
    }
    async babelTest(){
        return await new Promise((resolve => resolve('12345')))
    }
}