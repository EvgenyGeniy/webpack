const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const {CleanWebpackPlugin} = require('clean-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin')
module.exports = {
    mode:'development',
    context:path.resolve(__dirname,'src'),
    entry:{
        main:['@babel/polyfill','./index.js'],
        analytics:'./analytics.js'
    },
    output: {
        filename:'[name].[contenthash].js',
        path:path.resolve(__dirname,'dist')
    },
    devtool:'source-map',
    devServer:{
      port:4000
    },
    optimization:{
        minimizer:[
            new OptimizeCssAssetsPlugin(),
            new TerserPlugin()
        ],
        splitChunks:{
            chunks:'all'
        }
    },
    plugins:[
        new HtmlWebpackPlugin({
            template:'./index.html',
            minify:true
        }),
        new CleanWebpackPlugin(),
        new MiniCssExtractPlugin({
            filename:'[name].[contenthash].css'
        })
    ],
    module:{
        rules:[
            {
                test: /\.css$/,
                use:[MiniCssExtractPlugin.loader,'css-loader']
            },
            {
                test:/\.js$/,
                exclude: /node_modules/,
                use:{
                    loader:'babel-loader',
                    options:{
                        presets:[
                            '@babel/preset-env'
                        ]
                    }
                }

            }
        ]
    }
}